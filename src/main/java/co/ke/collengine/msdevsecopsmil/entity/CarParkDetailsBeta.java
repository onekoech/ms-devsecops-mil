package co.ke.collengine.msdevsecopsmil.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CarParkDetailsBeta {
    private String parkId;
    private int parkCapacity;

}
