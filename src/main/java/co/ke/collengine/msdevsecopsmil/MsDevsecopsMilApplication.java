package co.ke.collengine.msdevsecopsmil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsDevsecopsMilApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsDevsecopsMilApplication.class, args);
	}

}
