package co.ke.collengine.msdevsecopsmil.repository;

import co.ke.collengine.msdevsecopsmil.entity.CarParkDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarParkRepository extends CrudRepository<CarParkDetails, Long> {
    List<CarParkDetails> findCarParkDetailsByParkingName(String name);

}
